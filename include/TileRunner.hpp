#pragma once

#define TILERUNNER

#include <Arduino.h>

class TileRunner {
public:
    void init(int leftPinForward, int leftPinBackward, int rightPinForward, int rightPinBackward, int enablePin);

    String update(bool enable, double maxPower, int forwards, int turn);
    void stop();

private:
    int leftPinForward;
    int leftPinBackward;
    int rightPinForward;
    int rightPinBackward;
    int enablePin;

    static void writeMotor(int power, double maxPower, int forwardsPin, int backwardsPin);
};
