#pragma once

#define ROLLEYBOT

#include <Arduino.h>
#include <Servo.h>

class RolleyBot {
public:
    void init(int backLeftPin, int backRightPin, int frontLeftPin, int frontRightPin);

    String update(bool enable, double maxPower, int forwards, int turn, int strafe = 0);
    void stop();

private:
    Servo backLeftServo;
    Servo frontLeftServo;
    Servo backRightServo;
    Servo frontRightServo;

    static void writeMotor(int power, double maxPower, Servo& servo, bool reversed);
};
