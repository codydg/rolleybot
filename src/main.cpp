#include <Arduino.h>

#define USE_SERIAL false
#include "PrintUtils.hpp"
#include "UniversalRobotClient.hpp"

#include "TileRunner.hpp"

UniversalRobotClient client;
DataPacket packet;
unsigned long lastReadTime = 0;
unsigned long watchdogTimeout_ms = 200;

#ifdef TILERUNNER
#include "TileRunner.hpp"
#define LEFT_FORWARD_PIN    6
#define LEFT_BACKWARD_PIN   5
#define RIGHT_FORWARD_PIN   9
#define RIGHT_BACKWARD_PIN 10
#define ENABLE_PIN          4
TileRunner robot;
#endif
#ifdef ROLLEYBOT
#define BACK_LEFT_PIN    6
#define BACK_RIGHT_PIN   5
#define FRONT_LEFT_PIN   9
#define FRONT_RIGHT_PIN 10
RolleyBot robot;
#endif

void update(const DataPacket& packet);

void setup() {
    PrintUtils::init(115200);
#ifdef TILERUNNER
    robot.init(LEFT_FORWARD_PIN, LEFT_BACKWARD_PIN, RIGHT_FORWARD_PIN, RIGHT_BACKWARD_PIN, ENABLE_PIN);
#endif
#ifdef ROLLEYBOT
    robot.init(BACK_LEFT_PIN, BACK_RIGHT_PIN, FRONT_LEFT_PIN, FRONT_RIGHT_PIN);
#endif
    client.init();

    while (!client.read(packet)) {
        PrintUtils::println("Waiting for Radio Signal");
        delay(1000);
    }
    PrintUtils::println("Waiting for user to disable");
    while (packet.toggle_1) {
        client.read(packet);
    }
    PrintUtils::println("Waiting for user to enable");
    while (!packet.toggle_1) {
        client.read(packet);
    }
    client.zero(packet);
    client.setDeadzone(5);
}

void loop() {
    if (client.read(packet)) {
        // Packet Received
        lastReadTime = millis();
        update(packet);
    } else if (lastReadTime != 0 && millis() - lastReadTime > watchdogTimeout_ms) {
        // Lost connection
        PrintUtils::println("Lost Connection");
        robot.stop();
        lastReadTime = 0;
    }
}

void update(const DataPacket& packet) {
    int forwards = packet.left_y;
    int turn = packet.right_x;
    int strafe = packet.left_x;

    // Scale to cap at 100% power
    int maxVal = max(max(abs(forwards), abs(turn)), abs(strafe));
    if (maxVal > 255) {
        // Cannot use *= due to integer division
        forwards = forwards * 255 / maxVal;
        turn = turn * 255 / maxVal;
        strafe = strafe * 255 / maxVal;
    }

    // Divide by user's slow mode setting
    double maxPower = 1.0 / ((packet.toggle_3 ? 1.0 : 1.5) * (packet.toggle_4 ? 1.0 : 2.0));
    bool enable = packet.toggle_1;

#ifdef TILERUNNER
    String toPrint = "Forwards: " +
                     String(forwards) +
                     " | Turn: " +
                     String(turn) +
                     " | Max Power: " +
                     String(maxPower);

    toPrint += " | " + robot.update(enable, maxPower, forwards, turn);
#endif
#ifdef ROLLEYBOT
    String toPrint = "Forwards: " +
                     String(forwards) +
                     " | Turn: " +
                     String(turn) +
                     " | Strafe: " +
                     String(strafe);

    toPrint += " | " + robot.update(enable, maxPower, forwards, turn, strafe);
#endif

    PrintUtils::println(toPrint);
}
