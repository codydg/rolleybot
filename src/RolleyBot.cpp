#include "RolleyBot.hpp"

void RolleyBot::init(int backLeftPin, int backRightPin, int frontLeftPin, int frontRightPin) {
    backLeftServo.attach(backLeftPin);
    backRightServo.attach(backRightPin);
    frontLeftServo.attach(frontLeftPin);
    frontRightServo.attach(frontRightPin);

    backLeftServo.writeMicroseconds(1500);
    backRightServo.writeMicroseconds(1500);
    frontLeftServo.writeMicroseconds(1500);
    frontRightServo.writeMicroseconds(1500);
}

String RolleyBot::update(bool enable, double maxPower, int forwards, int turn, int strafe) {
    auto backLeft   = enable ? (forwards + turn - strafe) : 0;
    auto backRight  = enable ? (forwards - turn + strafe) : 0;
    auto frontLeft  = enable ? (forwards + turn + strafe) : 0;
    auto frontRight = enable ? (forwards - turn - strafe) : 0;

    writeMotor(backLeft  , maxPower, backLeftServo  , false);
    writeMotor(backRight , maxPower, backRightServo , true);
    writeMotor(frontLeft , maxPower, frontLeftServo , false);
    writeMotor(frontRight, maxPower, frontRightServo, true);

    return "BL: " + String(backLeft) + " | BR: " + String(backRight) + " | FL: " + String(frontLeft) + " | FR: " + String(frontRight) + (enable ? " | Enabled" : " | Disabled");
}

void RolleyBot::stop() {
    update(0.0, 0, 0, false);
}

void RolleyBot::writeMotor(int power, double maxPower, Servo& servo, bool reversed) {
    // Reversed parameter represents reversing the direction of the motor at all times.
    // Internal to the function, reversed is used to determine which pin to hold low and which to power.
    if (reversed) {
        power = -power;
    }
    int maxPowerInt = floor(maxPower * 255);
    auto absPower = abs(power);
    if (absPower > maxPowerInt) {
        power = power/absPower * maxPowerInt;
        absPower = abs(power);
    }
    power = map(power, -255, 255, 1100, 1900);
    servo.writeMicroseconds(power);
}
