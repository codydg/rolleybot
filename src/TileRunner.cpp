#include "TileRunner.hpp"

void TileRunner::init(int leftPinForward, int leftPinBackward, int rightPinForward, int rightPinBackward, int enablePin) {
    this->leftPinForward = leftPinForward;
    this->leftPinBackward = leftPinBackward;
    this->rightPinForward = rightPinForward;
    this->rightPinBackward = rightPinBackward;
    this->enablePin = enablePin;

    pinMode(this->leftPinForward, OUTPUT);
    pinMode(this->leftPinBackward, OUTPUT);
    pinMode(this->rightPinForward, OUTPUT);
    pinMode(this->rightPinBackward, OUTPUT);
    pinMode(this->enablePin, OUTPUT);
}

String TileRunner::update(bool enable, double maxPower, int forwards, int turn) {
    auto left  = enable ? (forwards + turn) : 0;
    auto right = enable ? (forwards - turn) : 0;

    digitalWrite(enablePin, enable ? HIGH : LOW);

    writeMotor(left , maxPower, leftPinForward , leftPinBackward );
    writeMotor(right, maxPower, rightPinForward, rightPinBackward);

    return "Left: " + String(left) + " | Right: " + String(right) + (enable ? " | Enabled" : " | Disabled");
}

void TileRunner::stop() {
    update(false, 0.0, 0, 0);
}

void TileRunner::writeMotor(int power, double maxPower, int forwardsPin, int backwardsPin) {
    int maxPowerInt = floor(maxPower * 255);
    auto absPower = abs(power);
    if (absPower > maxPowerInt) {
        power = power/absPower * maxPowerInt;
        absPower = abs(power);
    }
    if (power > 0) {
        analogWrite(backwardsPin, 0);
        analogWrite(forwardsPin, absPower);
    } else if (power < 0) {
        analogWrite(forwardsPin, 0);
        analogWrite(backwardsPin, absPower);
    } else {
        analogWrite(forwardsPin, 0);
        analogWrite(backwardsPin, 0);
    }
}
